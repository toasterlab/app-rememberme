import { Button, View, Text, ScrollView } from "react-native";
import React from "react";
import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  footnote: { fontSize: 9, fontWeight: "bold", color: "#808088" },
  bold: { fontWeight: "bold" },
  italic: { fontStyle: "italic" },
  underline: { textDecorationLine: "underline" },
  splash: {
    flex: 1,
    margin: 50,
    color: "#FFFFFF"
  }
});
const content = ctx => (
  <View
    style={{
      backgroundColor: "white",
      padding: 22,
      justifyContent: "center",
      alignItems: "center",
      borderRadius: 4,
      flex: 0.5,
      borderColor: "rgba(0, 0, 0, 0.1)"
    }}
  >
    <ScrollView>
      <Text>
        <Text style={styles.italic}>Remember Me</Text> is an interactive sound
        project that records and archives the sonic environment of the Prague
        Exhibition Grounds. The soundscapes of chosen locations around the
        Exhibition Grounds are documented through binaural recordings with
        time/date stamps, and photographs of the artist recording prior to the
        Prague Quadrennial Festival.
        {"\n"}
        {"\n"}
        Participants can journey to the chosen locations, position and orient
        themselves according to the photographs, and listen with headphones to
        their correlating recordings through the mobile Remember Me App. As the
        population increases during the festival, so does the noise pollution,
        inhibiting the environment’s soundscape to be present; the creaks of the
        rollercoaster when wind brushes against it, a local bird singing at
        11:52am on a Saturday, that moment of silence where nature and
        architecture stand still. When these moments are amplified, euphoria
        overwhelms its listeners into a blissful, calm state, feeling immersed
        and connected to their environment.
        {"\n"}
        {"\n"}
        <Text style={styles.italic}>Remember Me</Text> is an auditory voyage
        through time and space. The sounds that are indigenous to this land are
        digitally preserved allowing participants to experience the aural
        essence of the environment and to unearth the rich sonic history
        embedded within the Exhibition Grounds. These aural records remind us
        that this land has a past, monumental memories, and a voice with a story
        to tell.
        <Text style={styles.footnote}>
          {"\n"}
          {"\n"}
          {"\n"}A Toasterlab! Production{"\n"}
          Emplacement © 2019 The Place Lab Ltd.{"\n"}
          {ctx.props.appState.currentPackage === null
            ? "NONE"
            : ctx.props.appState.currentPackage}
          {"\n"}
          Tiles © Esri - Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye,
          Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community
        </Text>
      </Text>
    </ScrollView>
    <View style={{ marginTop: 20 }}>
      <Button
        style={{ color: "#12260B" }}
        title="OK"
        color="#12260B"
        onPress={() => {
          ctx.setState({ isModalVisible: !ctx.state.isModalVisible });
        }}
      />
    </View>
  </View>
);
export default content;
