import { Button, View, Text } from "react-native";
import React from "react";
import Progress from "../components/Progress";
const content = (ctx, styles) => (
  <View style={styles.splash_container}>
    <View style={styles.splash_container_inner}>
      <Text style={styles.splash}>
        <Text style={styles.italic}>Remember Me</Text> is an interactive sound
        project that records and archives the sonic environment of the Prague
        Exhibition Grounds. The soundscapes of chosen locations around the
        Exhibition Grounds are documented through binaural recordings with
        time/date stamps, and photographs of the artist recording prior to the
        Prague Quadrennial Festival.
        {"\n"}
        {"\n"}
        <Text style={styles.bold}>
          This app is now downloading content to your phone so you can explore
          without needing a network connection.
        </Text>
      </Text>
      {ctx.props.appState.progress.percentComplete <= 0 && (
        <Button
          onPress={ctx.onRetrieveLatestContent}
          title="Download Content"
          color="#FFFFFF"
          accessibilityLabel="Download Content"
        />
      )}
    </View>
    {ctx.props.appState.progress.percentComplete > 0 && (
      <Progress
        percentage={ctx.props.appState.progress.percentComplete}
        color={ctx.props.appState.progress.color}
      />
    )}
  </View>
);
export default content;
