// Decodes a JWT token

/*
Accepts single variable or an array of variables
Returns true if any of the provided values are
null, undefined or blank
*/

import AsyncStorage from "@react-native-community/async-storage";
export const setLocalStorage = async (key, val) => {
  try {
    await AsyncStorage.setItem(key, val);
  } catch (error) {
    console.error("FATAL: GET - Cannot access AsyncStorage!");
  }
};

export const getLocalStorage = async key => {
  try {
    const value = await AsyncStorage.getItem(key);
    return value;
  } catch (error) {
    console.error("FATAL: SET - Cannot access AsyncStorage!");
  }
};

export const missingValue = variable => {
  if (!Array.isArray(variable)) {
    variable = [variable];
  }

  let isMissing = false;
  variable.forEach(value => {
    if (!isMissing) {
      isMissing =
        value === null ||
        typeof value === "undefined" ||
        (typeof value === "string" && value.trim().length === 0);
    }
  });
  return isMissing;
};

export const decodeToken = token => {
  var base64Url = token.split(".")[1];
  var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  return JSON.parse(window.atob(base64));
};

export const exportDataToFile = (data, fileName, formatAsJSON) => {
  const a = document.createElement("a");
  const type = fileName.split(".").pop();
  a.href = URL.createObjectURL(
    new Blob([formatAsJSON ? JSON.stringify(data) : data], {
      type: `text/${type === "txt" ? "plain" : type}`
    })
  );
  a.download = fileName;
  a.click();
};

export const coerceJSON = string => {
  while (typeof string === "string") {
    string = JSON.parse(string);
  }
  return string;
};

export const toDataUrl = (url, callback) => {
  var xhr = new XMLHttpRequest();
  xhr.onload = function() {
    var reader = new FileReader();
    reader.onloadend = function() {
      callback(reader.result);
    };
    reader.readAsDataURL(xhr.response);
  };
  xhr.open("GET", url);
  xhr.responseType = "blob";
  xhr.send();
};

export const loadAsset = (filename, filetype) => {
  let fileref;
  switch (filetype) {
  case "js":
    fileref = document.createElement("script");
    fileref.setAttribute("type", "text/javascript");
    fileref.setAttribute("src", filename);
    break;

  case "css":
    fileref = document.createElement("link");
    fileref.setAttribute("rel", "stylesheet");
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", filename);
    break;

  default:
    console.log("I don't know how to load assets of type: " + filetype);
    return;
  }
  document.getElementsByTagName("head")[0].appendChild(fileref);
};

export const getJsonFromUrl = () => {
  var query = window.location.search.substr(1);
  var result = {};
  query.split("&").forEach(function(part) {
    var item = part.split("=");
    result[item[0].toLowerCase()] = decodeURIComponent(item[1]);
  });
  return result;
};

export const compareWithOptions = (a, b, isCaseSensitive, isExactMatch) => {
  if (
    a === null ||
    b === null ||
    (typeof a === "undefined" || typeof b === "undefined")
  ) {
    return false;
  }
  let retval = false;
  if (!isCaseSensitive) {
    a = a.toLowerCase();
    b = b.toLowerCase();
  }
  if (isExactMatch) {
    retval = a === b;
  } else {
    retval = a.includes(b);
  }
  return retval;
};
//https://stackoverflow.com/questions/979256/sorting-an-array-of-javascript-objects
/* "Sort by, then by"
// Based on this jsfiddle:
// http://jsfiddle.net/dFNva/1/
// as a response to this stackoverflow post:
// http://stackoverflow.com/a/979325/2502532
// Improvements include JSON deep access with path notation, 'then by' sorting.
// Description: Sort by object key, with optional reverse ordering, priming, and 'then by' sorting.

    array.sort(
        by(path[, reverse[, primer[, then]]])
    );
*/

/* THE FUNCTION */
export const sort_by = (path, reverse, primer, then) => {
  var get = function(obj, path) {
      if (path) {
        path = path.split(".");
        for (var i = 0, len = path.length - 1; i < len; i++) {
          obj = obj[path[i]];
        }
        return obj[path[len]];
      }
      return obj;
    },
    prime = function(obj) {
      return primer ? primer(get(obj, path)) : get(obj, path);
    };

  return function(a, b) {
    var A = prime(a),
      B = prime(b);

    return (
      (A < B ? -1 : A > B ? 1 : typeof then === "function" ? then(a, b) : 0) *
      [1, -1][+!!reverse]
    );
  };
};

//https://davidwalsh.name/javascript-debounce-function
// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
export const debounce = function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this,
      args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

//https://bradsknutson.com/blog/javascript-detect-mouse-leaving-browser-window/
export const addEvent = (obj, e, fn) => {
  if (obj.addEventListener) {
    obj.addEventListener(e, fn, false);
  } else if (obj.attachEvent) {
    obj.attachEvent("on" + e, fn);
  }
};

export const hexToRgb = hex => {
  // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
  var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, function(m, r, g, b) {
    return r + r + g + g + b + b;
  });

  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    }
    : null;
};

//https://gist.github.com/fupslot/5015897
export const dataURItoBlob = dataURI => {
  // convert base64 to raw binary data held in a string
  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
  var byteString = atob(dataURI.split(",")[1]);

  // separate out the mime component
  /*
	var mimeString = dataURI
		.split(",")[0]
		.split(":")[1]
		.split(";")[0];
*/
  // write the bytes of the string to an ArrayBuffer
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }

  // write the ArrayBuffer to a blob, and you're done
  var bb = new Blob([ab]);
  return bb;
};

export const makeAssetURL = url => {
  if (url.includes("http")) {
    return url;
  } else {
    return `${window._env_.REACT_APP_ASSET_SERVER}/${
      window._env_.REACT_APP_S3_APPID
    }/${url}`;
  }
};

export const getByID = (id, object) => {
  if (missingValue([id, object])) {
    console.warn("GetByID: Neither ID nor object should be null");
    return null;
  }
  let found = null;
  object.forEach(item => {
    if (parseInt(item.id) === parseInt(id)) {
      found = item;
    }
  });
  return JSON.parse(JSON.stringify(found));
};

export const formatFilesize = (sizeInBytes, includeLabel) => {
  let kB = Math.round(sizeInBytes * 0.001 * 100) / 100;
  let MB = Math.round(sizeInBytes * 0.000001 * 100) / 100;
  let GB = Math.round(sizeInBytes * 0.000000001 * 100) / 100;
  let TB = Math.round(sizeInBytes * 0.000000000001 * 100) / 100;

  let retval;
  if (TB >= 1) {
    retval = includeLabel ? `${TB} TB` : TB;
  } else if (GB >= 1) {
    retval = includeLabel ? `${GB} GB` : GB;
  } else if (MB >= 1) {
    retval = includeLabel ? `${MB} MB` : MB;
  } else if (kB >= 1) {
    retval = includeLabel ? `${kB} kB` : kB;
  } else {
    retval = includeLabel ? `${sizeInBytes} bytes` : sizeInBytes;
  }
  return retval;
};

export const dangerouslyLoadContentIntoDiv = (url, elementid, cb) => {
  fetch(url)
    .then(response => response.text())
    .then(html => {
      document.getElementById(elementid).innerHTML = html;
      cb();
    })
    .catch(error => {
      console.warn(error);
    });
};
