Remember Me is an interactive sound project that records and archives the sonic environment of the Prague Exhibition Grounds. The soundscapes of chosen locations around the Exhibition Grounds are documented through binaural recordings with time/date stamps, and photographs of the artist recording prior to the Prague Quadrennial Festival. Participants can journey to the chosen locations, position and orient themselves according to the photographs, and listen with headphones to their correlating recordings through the mobile Remember Me App. As the population increases during the festival, so does the noise pollution, inhibiting the environment’s soundscape to be present; the creaks of the rollercoaster when wind brushes against it, a local bird singing at 11:52am on a Saturday, that moment of silence where nature and architecture stand still. When these moments are amplified, euphoria overwhelms its listeners into a blissful, calm state, feeling immersed and connected to their environment. Remember Me is an auditory voyage through time and space. The sounds that are indigenous to this land are digitally preserved allowing participants to experience the aural essence of the environment and to unearth the rich sonic history embedded within the Exhibition Grounds. These aural records remind us that this land has a past, monumental memories, and a voice with a story to tell.

Fix ios remote debugger issue:

https://github.com/karanjthakkar/ReactNativeDevLoadingViewIssue/commit/e1a637cd82bf45e6af6814746d863b131b76ac62

watchman watch-del-all && rm -rf \$TMPDIR/react-\* && rm -rf node_modules/ && npm cache clean --force && npm install && npm start -- --reset-cache

/_
const location = "maptool-client-build/index.html";
const params = `platform=${Platform.OS}&cache=${Math.random()}`;
let injectedJS = `if (!window.location.search) { var link = document.getElementById('progress-bar'); link.href = './${location}?${params}'; link.click(); }`;
let sourceUri =
(Platform.OS === "android" ? "file:///android_asset/" : "") +
"Web.bundle/loader.html";
injectedJS = null;
_/

react-native run-ios --configuration Release --device "HELO_PHONE"
react-native run-ios --configuration Release

https://help.apple.com/app-store-connect/#/devd274dd925

for count in {1..5}; do dig +noall +answer s3.wasabisys.com | tail -1; echo -n `date`" -- ";curl -w 'bytesPerSecondDownload %{speed_download}\n' -so /dev/null https://s3.wasabisys.com/assets.toasterlab.xyz/2a1470e1-61f1-4de4-a805-d77cba6647f3/packages/1/build.zip; done

https://s3.wasabisys.com/assets.toasterlab.xyz/2a1470e1-61f1-4de4-a805-d77cba6647f3/packages/1/build.zip

sudo keytool -genkey -v -keystore rememberme.keystore -alias rememberme -keyalg RSA -keysize 2048 -validity 10000
nrt~jB6b&-5Sf8R

cd /Library/Java/JavaVirtualMachines/jdk-9.0.1.jdk/Contents/Home
cp rememberme.keystore /Users/andrew/Code/RN/app-rememberme/android/app

# Android release

You should be changing your versionCode and versionName in android/app/build.gradle

$cd android$ ./gradlew bundleRelease
The generated AAB can be found under android/app/build/outputs/bundle/release/app.aab, and is ready to be uploaded to Google Play.
https://facebook.github.io/react-native/docs/signed-apk-android

# Shake

adb shell input keyevent 82

# Watch build proces

watchbuild -a com.theplacelab.remembermepq -u dev@digitalscenographic.com

# Back button

https://github.com/facebook/react-native/issues/13775
