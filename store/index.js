import createSagaMiddleware from "redux-saga";
import rootSaga from "../sagas";
import rootReducer from "../reducers";
import { createStore, applyMiddleware } from "redux";

const sagaMiddleware = createSagaMiddleware();
export default initialState => {
  //return createStore(rootReducer, initialState);
  let store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
  sagaMiddleware.run(rootSaga);
  return store;
};
