import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  footnote: { fontSize: 9, fontWeight: "bold", color: "#808088" },
  bold: { fontWeight: "bold" },
  italic: { fontStyle: "italic" },
  underline: { textDecorationLine: "underline" },
  splash_container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#244B15"
  },
  splash_text: {
    flex: 1,
    margin: 50,
    marginTop: 100,
    color: "#FFFFFF",
    backgroundColor: "#244B15"
  },
  splashButton: {
    flex: 1
  },

  button: {
    color: "white",
    textAlign: "center",
    flex: 1,
    height: "30%",
    alignItems: "center"
  }
});

export default styles;
