import * as types from "./actionTypes";

// Action Helper
function action(type, payload = {}) {
  return { type, payload };
}
export const updateProgressBar = payload =>
  action(types.APP_PROGRESS_UPDATE, payload);

export const removeDialog = () => action(types.APP_ERROR_DIALOG_REMOVE);
export const checkForLocalPackage = payload => action(types.PKG_INIT, payload);
export const retrieveLatestPackage = () => action(types.PKG_RETRIEVE_LATEST);
export const startWebserver = payload => action(types.WEBSERVER_START, payload);
export const init = payload => action(types.APP_INIT, payload);
