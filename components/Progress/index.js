import PropTypes from "prop-types";
import React, { Component } from "react";
import { View } from "react-native";
type Props = {};

export default class Progress extends Component<Props> {
  render = () => {
    return (
      <View style={{ flexDirection: "row" }}>
        <View
          style={{
            width: `${this.props.percentage}%`,
            borderWidth: 0,
            height: 8,
            backgroundColor: this.props.color
          }}
        />
      </View>
    );
  };
}

Progress.propTypes = {
  percentage: PropTypes.number,
  backgroundColor: PropTypes.string,
  color: PropTypes.string
};
