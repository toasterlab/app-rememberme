// Webserver Sagas
import { put, select } from "redux-saga/effects";
import StaticServer from "react-native-static-server";
import uuid from "uuid";
import RNFS from "react-native-fs";
import * as actions from "../actions/actionTypes";
import { unzip } from "react-native-zip-archive";

export const start = function*() {
  const reduxState = yield select();

  // Refuse to start if we have no package at all
  if (reduxState.app.currentPackage === null) {
    console.error("No current package");
    return;
  }

  // Generate a new UUID for this content if necessary
  let contentInstanceID = null;
  let rebuildNecessary = false;
  try {
    contentInstanceID = yield RNFS.readFile(
      `${RNFS.DocumentDirectoryPath}/contentInstanceID.txt`,
      "utf8"
    );
    contentInstanceID = contentInstanceID.trim();
  } catch (e) {
    console.log(e);
  }
  if (contentInstanceID === null) {
    contentInstanceID = uuid.v4();
    rebuildNecessary = true;
    try {
      yield RNFS.writeFile(
        `${RNFS.DocumentDirectoryPath}/contentInstanceID.txt`,
        contentInstanceID,
        "utf8"
      );
    } catch (e) {
      console.log(e);
    }
  }

  // Stop the webserver if it's running
  if (typeof reduxState.app.webserver.server !== "undefined") {
    reduxState.app.webserver.server.stop();
  }

  let path = `${RNFS.DocumentDirectoryPath}/www/${contentInstanceID}`;

  let wwwExists = yield RNFS.exists(`${RNFS.DocumentDirectoryPath}/www`);
  if (!wwwExists) {
    try {
      yield RNFS.mkdir(path);
    } catch (e) {
      console.log(e);
    }
  }

  // Rebuild
  if (rebuildNecessary) {
    console.log("Rebuilding...");
    try {
      yield RNFS.unlink(`${RNFS.DocumentDirectoryPath}/www/`);
    } catch (e) {
      console.log(e);
    }
    try {
      yield RNFS.mkdir(path);
      yield RNFS.mkdir(`${RNFS.DocumentDirectoryPath}/downloads/`);
    } catch (e) {
      console.log(e);
    }

    let unpackedBuild, unpackedAssets, www_build, www_assets;
    if (reduxState.app.currentPackage === "PRELOAD") {
      console.log("Unpacking PRELOAD...");
      try {
        yield unzip(
          `${RNFS.DocumentDirectoryPath}/preload/content.zip`,
          `${RNFS.DocumentDirectoryPath}/preload`
        );

        yield unzip(
          `${RNFS.DocumentDirectoryPath}/preload/build.zip`,
          `${RNFS.DocumentDirectoryPath}/preload/build`
        );

        unpackedBuild = `${RNFS.DocumentDirectoryPath}/preload/build/build`;
        unpackedAssets = `${RNFS.DocumentDirectoryPath}/preload/assets`;
        www_build = `${
          RNFS.DocumentDirectoryPath
        }/www/${contentInstanceID}/build`;
        www_assets = `${
          RNFS.DocumentDirectoryPath
        }/www/${contentInstanceID}/assets`;
      } catch (e) {
        console.log(e);
      }
    } else {
      console.log(`Unpacking ${reduxState.app.currentPackage}...`);

      try {
        yield unzip(
          reduxState.app.pkg.config.content_zip,
          reduxState.app.pkg.config.content_path
        );
        yield unzip(
          reduxState.app.pkg.config.client_zip,
          reduxState.app.pkg.config.client_path
        );
      } catch (e) {
        console.log(e);
      }

      unpackedBuild = `${reduxState.app.pkg.config.client_path}/build`;
      unpackedAssets = `${reduxState.app.pkg.config.content_path}`;
      www_build = `${
        RNFS.DocumentDirectoryPath
      }/www/${contentInstanceID}/build`;
      www_assets = `${
        RNFS.DocumentDirectoryPath
      }/www/${contentInstanceID}/assets`;
    }

    try {
      // RNFS.copyFile didn't work on Android (WTF?) so refactored to move.
      yield RNFS.moveFile(unpackedBuild, www_build);
      yield RNFS.moveFile(unpackedAssets, www_assets);
    } catch (e) {
      console.log(e);
    }
  }

  yield put({
    type: actions.PKG_CHECK_FOR_UPDATE
  });

  const server = new StaticServer(8080, `${RNFS.DocumentDirectoryPath}/www/`, {
    localOnly: true
  });

  let url = yield server.start();

  yield put({
    type: actions.WEBSERVER_UPDATE_STATUS,
    payload: {
      server: server,
      isRunning: true,
      host: url,
      path: path,
      contentInstanceID: contentInstanceID
    }
  });

  yield put({
    type: actions.APP_PROGRESS_UPDATE,
    payload: { inProgress: false }
  });

  console.log(`🕷:${url} | ${contentInstanceID}`);
};
