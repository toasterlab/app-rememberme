import { takeLatest, all } from "redux-saga/effects";
import * as app from "./saga_app.js";
import * as pkg from "./saga_pkg.js";
import * as webserver from "./saga_webserver.js";
import * as actions from "../actions/actionTypes";

// Export only a single rollup point
export default function* rootSaga() {
  yield all([
    takeLatest(actions.APP_ERROR, app.onError),

    takeLatest(actions.APP_INIT, app.init),
    takeLatest(actions.WEBSERVER_START, webserver.start),

    takeLatest(actions.PKG_GET_CONTENT, pkg.getContent),
    takeLatest(actions.PKG_GET_CLIENT, pkg.getClient),
    takeLatest(actions.PKG_CHECK_FOR_UPDATE, pkg.onCheckForUpdate),

    takeLatest(actions.PKG_RETRIEVE_LATEST, pkg.onRetrieveLatest)
  ]);
}
