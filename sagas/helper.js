import { missingValue } from "../utility";

const ANON_HEADERS = {
  Accept: "application/json, application/xml, text/plain, text/html, *.*",
  "Content-Type": "application/json; charset=utf-8"
};

export const signed = (method, url, token, body) => {
  if (missingValue(body)) {
    return fetch(url, {
      method: method,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json; charset=utf-8"
      }
    });
  } else {
    return fetch(url, {
      method: method,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json; charset=utf-8"
      },
      body: body
    });
  }
};

export const anonymous = (method, url, body) => {
  if (missingValue(body)) {
    return fetch(url, {
      method: method,
      headers: ANON_HEADERS
    });
  } else {
    return fetch(url, {
      method: method,
      headers: ANON_HEADERS,
      body: body
    });
  }
};
