// APP Sagas
import RNFS from "react-native-fs";
import * as actions from "../actions/actionTypes";
import { put } from "redux-saga/effects";
import { DocumentDirectoryPath } from "react-native-fs";
import { unzip } from "react-native-zip-archive";
import { Platform } from "react-native";

export const init = function*() {
  console.log(`OPERATING IN: ${DocumentDirectoryPath}`);

  let currentPackageID = null;
  try {
    currentPackageID = yield RNFS.readFile(
      `${DocumentDirectoryPath}/pid.txt`,
      "utf8"
    );
    currentPackageID = currentPackageID.trim();
  } catch (e) {
    //console.log(e);
  }

  currentPackageID = currentPackageID === null ? "PRELOAD" : currentPackageID;
  yield put({
    type: actions.PKG_SET_ID,
    payload: currentPackageID
  });

  if (currentPackageID === "PRELOAD") {
    console.log("Preloading content...");

    let preloadExists = yield RNFS.exists(
      `${RNFS.DocumentDirectoryPath}/preload`
    );
    if (!preloadExists) {
      if (Platform.OS === "android") {
        // Android (should put preload.zip in assets folder)
        yield RNFS.copyFileAssets(
          "preload.zip",
          `${RNFS.DocumentDirectoryPath}/preload.zip`
        );
        yield unzip(
          `${RNFS.DocumentDirectoryPath}/preload.zip`,
          `${RNFS.DocumentDirectoryPath}/`
        );
      } else {
        // ios (should put preload.zip in bundle)
        yield unzip(
          `${RNFS.MainBundlePath}/preload.zip`,
          `${RNFS.DocumentDirectoryPath}/`
        );
      }
      yield RNFS.copyFile(
        `${RNFS.DocumentDirectoryPath}/preload/pid.txt`,
        `${RNFS.DocumentDirectoryPath}/pid.txt`
      );
    }
  }

  if (currentPackageID !== "PRELOAD" && currentPackageID !== null) {
    try {
      yield RNFS.stat(
        `${RNFS.DocumentDirectoryPath}/downloads/${currentPackageID}`
      );
    } catch (e) {
      currentPackageID = null;
    }
  }
  try {
    yield RNFS.mkdir(`${RNFS.DocumentDirectoryPath}/downloads/`);
  } catch (e) {
    console.log(e);
  }

  console.log(`Starting with...(${currentPackageID})`);
  yield put({ type: actions.WEBSERVER_START });
};

export const onError = effect => {
  console.error(
    "😫 FATAL ERROR (" + effect.payload.source + ") " + effect.payload.error
  );
  return effect;
};
