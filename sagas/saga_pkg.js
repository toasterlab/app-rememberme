// PKG Sagas
import * as actions from "../actions/actionTypes";
import { select, put } from "redux-saga/effects";
import RNFetchBlob from "rn-fetch-blob";
import * as helper from "./helper";
import RNFS from "react-native-fs";
import { DocumentDirectoryPath } from "react-native-fs";

export const onCheckForUpdate = function*() {
  try {
    const reduxState = yield select();
    yield put({
      type: actions.PKG_UPDATE_STATUS,
      payload: false
    });
    let manifestURL = `${
      reduxState.app.pkg.config.baseURL
    }/manifest.json?${Math.random()}`;
    const response = yield helper.anonymous("GET", manifestURL);
    if (response.ok) {
      let response_json = yield response.json();
      let pkgID = response_json.published;
      if (pkgID !== reduxState.app.currentPackage) {
        yield put({
          type: actions.PKG_UPDATE_AVAILABLE,
          payload: { available: true, info: response_json }
        });
        return;
      }
    }
    yield put({
      type: actions.PKG_UPDATE_AVAILABLE,
      payload: { available: false, info: {} }
    });
  } catch (e) {
    yield put({
      type: actions.PKG_UPDATE_AVAILABLE,
      payload: { available: false, info: {} }
    });
  }
};

// Called via getContent
export const getClient = function*(action) {
  const reduxState = yield select();

  yield put({
    type: actions.APP_PROGRESS_UPDATE,
    payload: {
      color: "#BADA55",
      inProgress: true,
      percentComplete: 1,
      steps: "(2/2)"
    }
  });

  let x = yield RNFetchBlob.config({
    fileCache: true,
    path: reduxState.app.pkg.config.client_zip
  });
  let currentProgress = 1;
  yield x
    .fetch("GET", reduxState.app.pkg.config.clientURL)
    .progress((received, total) => {
      let progress = Math.round((received / total) * 100);
      if (currentProgress < progress) {
        reduxState.app.events.emit("progress", progress);
      }
      currentProgress = progress;
    });

  yield put({ type: actions.APP_INIT });
  yield put({
    type: actions.APP_PROGRESS_UPDATE,
    payload: { percentComplete: 100 }
  });
};

export const getContent = function*(action) {
  const packageInfo = action.payload;
  const reduxState = yield select();

  try {
    yield RNFS.unlink(`${RNFS.DocumentDirectoryPath}/contentInstanceID.txt`);
  } catch (e) {
    console.log(e);
  }

  yield put({
    type: actions.APP_PROGRESS_UPDATE,
    payload: {
      color: "#BADA55",
      inProgress: true,
      percentComplete: 1,
      steps: "(1/2)"
    }
  });

  let x = yield RNFetchBlob.config({
    fileCache: true,
    path: reduxState.app.pkg.config.content_zip
  });

  let currentProgress = 0;
  yield x.fetch("GET", packageInfo.downloadURL).progress((received, total) => {
    let progress = Math.round((received / total) * 100);
    if (currentProgress < progress) {
      reduxState.app.events.emit("progress", progress - 1);
    }
    currentProgress = progress;
  });

  yield put({
    type: actions.PKG_UPDATE_STATUS,
    payload: true
  });

  yield put({ type: actions.PKG_GET_CLIENT });
};

export const onRetrieveLatest = function*(action) {
  try {
    const reduxState = yield select();

    let manifestURL = `${
      reduxState.app.pkg.config.baseURL
    }/manifest.json?${Math.random()}`;
    const response = yield helper.anonymous("GET", manifestURL);
    if (response.ok) {
      let response_json = yield response.json();
      let pkgID = response_json.published;
      let packageInfo;
      Object.keys(response_json.packages).forEach(pkg => {
        if (pkg === pkgID) {
          packageInfo = response_json.packages[pkg];
          if (typeof packageInfo !== "undefined") {
            packageInfo.downloadURL = `${
              reduxState.app.pkg.config.baseURL
            }/${pkgID}.zip`;
            put({
              type: actions.PKG_UPDATE_STATUS,
              payload: false
            });
          }
        }
      });
      if (reduxState.app.currentPackage !== pkgID) {
        try {
          yield RNFS.unlink(
            `${reduxState.app.pkg.config.content_path}/${
              reduxState.app.currentPackage
            }`
          );
        } catch (e) {
          console.log(e);
        }
        try {
          yield RNFS.unlink(`${DocumentDirectoryPath}/pid.txt`);
        } catch (e) {
          console.log(e);
        }
      }

      try {
        yield RNFS.writeFile(`${DocumentDirectoryPath}/pid.txt`, pkgID, "utf8");
      } catch (e) {
        console.log(e);
      }
      yield put({
        type: actions.PKG_SET_ID,
        payload: pkgID
      });

      let newPID = null;
      try {
        newPID = yield RNFS.readFile(
          `${DocumentDirectoryPath}/pid.txt`,
          "utf8"
        );
        newPID = newPID.trim();
      } catch (e) {
        //console.log(e);
      }
      console.log("new PID..." + newPID);

      if (typeof packageInfo !== "undefined") {
        yield put({
          type: actions.PKG_GET_CONTENT,
          payload: packageInfo
        });
      }
    } else {
      yield put({
        type: actions.APP_ERROR,
        payload: { error: response, source: action.type }
      });
    }
  } catch (e) {
    //console.log(e);
    yield put({
      type: actions.APP_ERROR_DIALOG,
      payload: { error: e, source: action.type }
    });
  }
};
