import PropTypes from "prop-types";
import React, { Component } from "react";
import { View, Text, ActivityIndicator, BackHandler } from "react-native";
import Button from "./components/Button";
import Progress from "./components/Progress";
import { WebView } from "react-native-webview";
import * as actions from "./actions";
import Modal from "react-native-modal";
import { connect } from "react-redux";
import Dialog from "react-native-dialog";
import { formatFilesize } from "./utility";
import moment from "moment";
import aboutContent from "./utility/aboutContent.js";
import styles from "./App_styles.js";
type Props = {};

class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      dialog: { visible: false },
      skipUpdateDialog: false,
      isWebviewVisible: false
    };
  }

  componentWillMount = () => {
    setTimeout(() => {
      this.setState({ isWebviewVisible: true });
    }, 1000);
    this.props.dispatch(actions.init());
    this.props.appState.events.on("content_unzipped", () => {
      this.props.dispatch(actions.init());
    });
    this.props.appState.events.on("progress", percent => {
      this.props.dispatch(
        actions.updateProgressBar({ percentComplete: percent })
      );
    });
    //BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  };

  componentWillUnmount = () => {
    this.props.appState.events.removeListener("content_unzipped");
    //BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
  };
  onRetrieveLatestContent = () => {
    this.props.dispatch(actions.retrieveLatestPackage());
  };
  /*
  onBackPress = () => {
    BackHandler.exitApp();
    // Return true to enable back button over ride.
    return true;
  };
*/
  render = () => {
    let statusMessage = "Rendering Map...";
    if (this.props.appState.progress.inProgress) {
      statusMessage = `Updating ${this.props.appState.progress.steps} (${
        this.props.appState.progress.percentComplete
      }%)...`;
    }

    let dialog = this.state.dialog;
    if (this.props.appState.errordialog !== null) {
      dialog = {
        ...this.props.appState.errordialog,
        visible: true,
        confirm_label: "OK",
        onConfirm: () => {
          this.props.dispatch(actions.removeDialog());
        },
        cancel_label: "Retry",
        onCancel: () => {
          this.props.dispatch(actions.removeDialog());
          this.onRetrieveLatestContent();
        }
      };
    }

    let sourceUri = `${this.props.appState.webserver.host}/${
      this.props.appState.webserver.contentInstanceID
    }/build/index.html?id=${this.props.appState.webserver.contentInstanceID}`;
    return (
      <View
        style={{ flex: 1, flexDirection: "column", backgroundColor: "#12260B" }}
      >
        {dialog.visible && (
          <Dialog.Container visible={dialog.visible}>
            <Dialog.Title>{dialog.title}</Dialog.Title>
            <Dialog.Description>{dialog.content}</Dialog.Description>
            {dialog.onCancel !== null && (
              <Dialog.Button
                label={dialog.cancel_label}
                onPress={dialog.onCancel}
              />
            )}
            {dialog.onConfirm !== null && (
              <Dialog.Button
                label={dialog.confirm_label}
                onPress={dialog.onConfirm}
              />
            )}
          </Dialog.Container>
        )}

        <Modal isVisible={this.state.isModalVisible}>
          {aboutContent(this, styles)}
        </Modal>

        {this.props.appState.webserver.isRunning && (
          <WebView
            style={{
              flex: 1,
              flexDirection: "row",
              backgroundColor: "#12260B",
              opacity: this.state.isWebviewVisible ? 1 : 0
            }}
            source={{ uri: sourceUri }}
            javaScriptEnabled={true}
            originWhitelist={["*"]}
            allowFileAccess={true}
            backgroundColor="#12260B"
            cacheEnabled={false}
          />
        )}

        {!this.props.appState.webserver.isRunning && (
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#12260B"
            }}
          >
            <ActivityIndicator size="small" color="#FFFFFF" />
          </View>
        )}
        {this.props.appState.progress.inProgress && (
          <Progress
            percentage={this.props.appState.progress.percentComplete}
            color={this.props.appState.progress.color}
          />
        )}
        <View
          style={{
            flexDirection: "row",
            justifyContent: "flex-start",
            backgroundColor: "#244B15"
          }}
        >
          {(this.props.appState.progress.inProgress ||
            !this.props.appState.webserver.isRunning) && (
            <View
              style={{
                flex: 1,
                height: 80,
                alignItems: "center",
                textAlign: "center"
              }}
            >
              <Text
                style={{ color: "white", marginTop: 21, fontWeight: "bold" }}
              >
                {statusMessage}
              </Text>
            </View>
          )}
          {!this.props.appState.progress.inProgress &&
            this.props.appState.webserver.isRunning && (
              <>
                <View
                  style={{ flex: 1, alignItems: "center", textAlign: "center" }}
                >
                  <Button
                    label="About"
                    onPress={() => {
                      this.setState({
                        isModalVisible: !this.state.isModalVisible
                      });
                    }}
                  />
                </View>
                <View style={{ flex: 1 }} />
                <View
                  style={{ flex: 1, alignItems: "center", textAlign: "center" }}
                >
                  <Button
                    highlight={this.props.appState.update.available}
                    label="Update"
                    onPress={this.onUpdateContent}
                  />
                </View>
              </>
          )}
        </View>
      </View>
    );
  };

  onUpdateContent = () => {
    if (!this.state.dialog.visible) {
      if (this.props.appState.update.available) {
        let updatePackageInfo = this.props.appState.update.info.packages[
          this.props.appState.update.info.published
        ];
        let dialogContent = "Something is wrong.";
        if (typeof updatePackageInfo !== "undefined") {
          dialogContent = `Updated ${moment(
            updatePackageInfo.packageDate
          ).fromNow()}.${"\n"}${formatFilesize(
            updatePackageInfo.sizeInBytes,
            true
          )}${"\n"}${"\n"}Do you want to download it now?`;
        }
        this.setState({
          dialog: {
            visible: true,
            cancel_label: "No",
            confirm_label: "Yes",
            onConfirm: () => {
              this.setState({
                dialog: { ...this.state.dialog, visible: false }
              });
              this.onRetrieveLatestContent();
            },
            onCancel: () => {
              this.setState({
                dialog: { ...this.state.dialog, visible: false }
              });
            },
            title: "New content available!",
            content: dialogContent
          }
        });
      } else {
        this.setState({
          dialog: {
            visible: true,
            title: "Up to date!",
            content: "You already have the latest content.",
            cancel_label: "Refresh",
            confirm_label: "OK",
            onConfirm: () => {
              this.setState({
                dialog: { ...this.state.dialog, visible: false }
              });
            },
            onCancel: () => {
              this.setState({
                dialog: { ...this.state.dialog, visible: false }
              });
              this.onRetrieveLatestContent();
            }
          }
        });
      }
      this.setState({
        isDialogVisible: true
      });
    }
  };
}

App.propTypes = {
  appState: PropTypes.object,
  dispatch: PropTypes.func
};

function appStateToProps(reduxState) {
  return {
    appState: reduxState.app
  };
}

export default connect(appStateToProps)(App);
