package com.rememberme;

import com.facebook.react.ReactActivity;

public class MainActivity extends ReactActivity {
    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "RememberMe";
    }

    //https://github.com/facebook/react-native/issues/13775
    @Override
    public void invokeDefaultOnBackPressed() {
        // do not call super.invokeDefaultOnBackPressed() as it will close the app.  Instead lets just put it in the background.
        moveTaskToBack(true);
    }
}
