import initialState from "./initialState_app";
import * as actionType from "../actions/actionTypes";

export default function api(state = initialState, action) {
  switch (action.type) {
  case actionType.PKG_INIT_CONTENT:
    return {
      ...state,
      initialized: true
    };

  case actionType.APP_ERROR_DIALOG_REMOVE:
    return {
      ...state,
      errordialog: null
    };

  case actionType.APP_ERROR_DIALOG:
    return {
      ...state,
      errordialog: {
        confirm_label: "OK",
        title: "Cannot reach the server",
        content: "Are you online?"
      }
    };

  case actionType.PKG_INIT_CONTENT__RESPONSE:
    return {
      ...state,
      firstTime: false,
      pkg: {
        ...state.pkg,
        data: {
          ...state.pkg.data,
          config:
              action.payload.config !== null
                ? JSON.parse(action.payload.config)[0]
                : null
        }
      }
    };

  case actionType.PKG_SET_ID:
    return {
      ...state,
      currentPackage: action.payload
    };

  case actionType.PKG_UPDATE_AVAILABLE:
    return {
      ...state,
      update: {
        available: action.payload.available,
        info: action.payload.info
      }
    };

  case actionType.APP_PROGRESS_UPDATE:
    return {
      ...state,
      progress: {
        ...state.progress,

        inProgress:
            typeof action.payload.inProgress !== "undefined"
              ? action.payload.inProgress
              : state.progress.inProgress,
        percentComplete:
            action.payload.percentComplete === 100
              ? 0
              : action.payload.percentComplete,
        color:
            typeof action.payload.color !== "undefined"
              ? action.payload.color
              : state.progress.color,
        steps:
            typeof action.payload.steps !== "undefined"
              ? action.payload.steps
              : state.progress.steps
      }
    };

  case actionType.PKG_UPDATE_STATUS:
    return {
      ...state,
      pkg: {
        ...state.pkg,
        status: {
          isReady: action.payload
        }
      }
    };

  case actionType.WEBSERVER_UPDATE_STATUS:
    return {
      ...state,
      webserver: action.payload
    };

  default:
    return state;
  }
}
