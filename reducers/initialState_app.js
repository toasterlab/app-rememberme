import EventEmitter from "eventemitter3";
import { DocumentDirectoryPath } from "react-native-fs";

const appID = "2a1470e1-61f1-4de4-a805-d77cba6647f3";
const mapID = "1";
const assetServerURL = "https://s3.wasabisys.com/assets.toasterlab.xyz";
export default {
  events: new EventEmitter(),
  initialized: false,
  title: "Remember Me",
  publisher: "Toasterlab",
  appID: appID,
  firstTime: true,
  currentPackage: null,
  update: { available: false, info: {} },
  errordialog: null,
  progress: {
    percentComplete: 0,
    color: "#BADA55",
    inProgress: false
  },
  webserver: {
    isRunning: false,
    host: null,
    path: null
  },
  pkg: {
    config: {
      baseURL: `${assetServerURL}/${appID}/packages/${mapID}`,
      www_path: `${DocumentDirectoryPath}/www`,
      content_zip: `${DocumentDirectoryPath}/downloads/content.zip`,
      content_path: `${DocumentDirectoryPath}/downloads/assets`,
      clientURL: `${assetServerURL}/${appID}/packages/${mapID}/build.zip`,
      client_zip: `${DocumentDirectoryPath}/downloads/build.zip`,
      client_path: `${DocumentDirectoryPath}/downloads/build`
    },
    data: {
      config: null,
      poi: null
    }
  }
};
